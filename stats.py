import re
import os
import bz2
from bz2 import decompress
import wget

print('Downloading team and user stat flat files...')

urls = ['https://apps.foldingathome.org/daily_user_summary.txt.bz2', 
'https://apps.foldingathome.org/daily_team_summary.txt.bz2']

path = os.getcwd() + '/files/'

for url in urls:
	filename = path + url.split('/')[3]

	print('Checking if file exists...')
	print(filename)


	if os.path.exists(filename):
		print("Delete old flat file...")
  		os.remove(filename)
	else:
  		print("The file does not exist...")

  	print('Downloading file...')
	wget.download(url, filename)

file0 = path + '/daily_team_summary.txt.bz2'
file1 = path + '/daily_user_summary.txt.bz2'

teamfile = bz2.BZ2File(file0) # open the file
userfile = bz2.BZ2File(file1)

teams = teamfile.readlines()
users = userfile.readlines()

teamid = '237112'

print('\n')
print('============')
print('TEAM STATS')
print('============')

for team in teams:
	if teamid in team:
		teaminfo = re.split(r'\t+', team.rstrip('\t\n'))
		print('team id: ' + teaminfo[0])
		print('team name: ' + teaminfo[1])
		print('total score: ' + teaminfo[2])
		print('total work units: ' + teaminfo[3])

print('\n')
print('============')
print('USER STATS')		
print('============')

for user in users:
	userinfo = re.split(r'\t+', user.rstrip('\t\n'))
	if teamid in userinfo:
		print('username: ' + userinfo[0])	
		print('credit: ' + userinfo[1])
		print('total work units: ' + userinfo[2])
		print('---------------')

		

